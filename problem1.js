function findEmailAddressesOfAll(arrayOfObjects)
{
    if(!Array.isArray(arrayOfObjects))
    {
        return "The given data is not valid";
    }

    let listOfEmailAddresses=[];

    arrayOfObjects.forEach((item)=> listOfEmailAddresses.push(item["email"]));

    return listOfEmailAddresses;
}

module.exports=findEmailAddressesOfAll;