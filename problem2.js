function findTheHobbies(arrayOfObjects,age)
{
    if(!Array.isArray(arrayOfObjects))
    {
        return "The given data is not valid";
    }

    let listOfHobbies=[];
    (arrayOfObjects.filter((item)=>item["age"]===age)).forEach((item)=> listOfHobbies.push(item["hobbies"]));

    return listOfHobbies;
}

module.exports=findTheHobbies;