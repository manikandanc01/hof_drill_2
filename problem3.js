function findTheStudentLiveInAustralia(arrayOfObjects)
{
    if(!Array.isArray(arrayOfObjects))
    {
        return "The given data is not valid";
    }

    let listOfIndividuals=arrayOfObjects.filter((item)=> item["isStudent"]===true && item["country"].toLowerCase()==="australia");

    return listOfIndividuals;
}

module.exports=findTheStudentLiveInAustralia;