function findNameAndCity(arrayOfObjects,position)
{
    if(!Array.isArray(arrayOfObjects))
    {
        return "The given data is not valid";
    }

    if(arrayOfObjects.length<=position)
    {
        return "Number of persons are less than the index"+position;
    }

    return [arrayOfObjects[position].name,arrayOfObjects[position].city];
    
}

module.exports=findNameAndCity;