function printAllPersonsAge(arrayOfObjects)
{
    if(!Array.isArray(arrayOfObjects))
    {
        return "The given data is not valid";
    }

    arrayOfObjects.forEach((item)=>console.log(item["age"]));
}

module.exports=printAllPersonsAge;