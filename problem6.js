function printFirstHobbyOfAllPersons(arrayOfObjects)
{
    if(!Array.isArray(arrayOfObjects))
    {
        return "The given data is not valid";
    }

    arrayOfObjects.forEach((item)=> console.log(item["hobbies"][0]));
    
}

module.exports=printFirstHobbyOfAllPersons;