function printNamesAndEmail(arrayOfObjects,age)
{
    if(!Array.isArray(arrayOfObjects))
    {
        return "The given data is not valid";
    }
    
    arrayOfObjects.filter((item)=>item["age"]===age).forEach((item)=>console.log(item["name"]+" "+item["email"]));
}

module.exports=printNamesAndEmail;