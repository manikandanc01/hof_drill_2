function printCountryAndCity(arrayOfObjects)
{
    if(!Array.isArray(arrayOfObjects))
    {
        return "The given data is not valid";
    }

    arrayOfObjects.forEach((item)=> console.log("City = "+item["city"]+", Country = "+item["country"]));
    
}
module.exports=printCountryAndCity;